import { Router } from 'express';
import MoviesController from '../controllers/MoviesController';

const router = Router();

router.get('/api/get-movies', MoviesController.findTopRatedMovies);
router.get('/api/movies', MoviesController.getAllMovies);

export default router;
