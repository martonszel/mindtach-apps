"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Movie = void 0;
const sequelize_1 = require("sequelize");
const dbconfig_1 = __importDefault(require("../config/dbconfig"));
class Movie extends sequelize_1.Model {
}
exports.Movie = Movie;
Movie.init({
    tmdbId: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
    },
    title: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        defaultValue: null,
    },
    length: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        defaultValue: null,
    },
    genres: {
        type: sequelize_1.DataTypes.ARRAY(sequelize_1.DataTypes.STRING),
        allowNull: true,
        defaultValue: null,
    },
    releaseDate: {
        type: sequelize_1.DataTypes.DATE,
        allowNull: true,
        defaultValue: null,
        validate: {
            isDate: {
                args: true,
                msg: 'Please enter correct date format',
            },
        },
    },
    overview: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
        defaultValue: null,
    },
    tmdbVoteAvarage: {
        type: sequelize_1.DataTypes.FLOAT,
        allowNull: false,
        defaultValue: null,
    },
    tmdbVoteCount: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        defaultValue: null,
    },
    posterUrl: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        defaultValue: null,
    },
    tmdbUrl: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        defaultValue: null,
    },
    directorId: {
        type: sequelize_1.DataTypes.INTEGER,
    },
    directorName: {
        type: sequelize_1.DataTypes.STRING,
        defaultValue: null,
    },
    directorBio: {
        type: sequelize_1.DataTypes.TEXT,
        defaultValue: null,
    },
    directorBirthday: {
        type: sequelize_1.DataTypes.DATEONLY,
        allowNull: true,
        defaultValue: null,
        validate: {
            isDate: {
                args: true,
                msg: 'Please enter correct date format',
            },
        },
    },
}, {
    tableName: 'movie',
    underscored: true,
    timestamps: true,
    sequelize: dbconfig_1.default,
    indexes: [
        {
            unique: true,
            fields: ['tmdb_id', 'director_id'],
        },
    ],
});
exports.default = Movie;
