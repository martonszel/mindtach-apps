"use strict";
const add = (a, b) => {
    return a + b;
};
describe('this is a test', () => {
    it('should pass', () => {
        expect(add(1, 2)).toBe(3);
    });
});
