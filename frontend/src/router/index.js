import { createRouter, createWebHistory } from 'vue-router'
import Movies from '/src/components/Movies.vue'
import Director from '/src/components/Director.vue'
const routes = [
    {
        path: '/',
        name: 'Movies',
        component: Movies,
        meta: {
            keepAlive: true
        }
    },

    {
        path: '/director',
        name: 'Director',
        component: Director,
        meta: {
            KeepAlive: false // No need to be cached
        }
    },
]
const router = createRouter({
    history: createWebHistory(),
    routes,
})
export default router