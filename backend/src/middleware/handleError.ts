import type { ErrorRequestHandler } from 'express';
import { Request, Response, NextFunction } from 'express';

// eslint-disable-next-line
const errorHandler: ErrorRequestHandler = (error: any, req: Request, res: Response, next: NextFunction) => {
	console.log(error);

	const status = error.status || 500;
	const message = error.message || 'Something went wrong';
	const route = req.path;
	const { stack } = error;
	const reqMethod = req.method;
	const reqIp = req.socket.remoteAddress;

	res.status(status).json({ status, message, route, stack });
};

export default errorHandler;
