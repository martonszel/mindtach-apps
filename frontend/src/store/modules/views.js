import apiClient, { getMovies } from '@/services/EventService.js';


const state = {
    name: 'Maki',
    movies: []
};

const getters = {};

const actions = {
    async getAllMovies({ commit }) {
        await getMovies({
        }).then((response) => {
            commit('SET_MOVIES', response.data)
        })
    },

};

const mutations = {
    SET_MOVIES(state, userdata) {
        state.movies = userdata
    }


};

export default {
    state,
    getters,
    actions,
    mutations
};