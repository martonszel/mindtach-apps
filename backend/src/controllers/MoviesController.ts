import { Request, Response, NextFunction } from 'express';
import axios from 'axios';
import Movie from '../models/Movie';
import CustomError from '../utilities/CustomError';

class MoviesController {
	findTopRatedMovies = async (req: Request, res: Response, next: NextFunction) => {
		let API_KEY = 'bd9eb422bf109b790455909c7396f70d';
		let BASE_URL = 'https://api.themoviedb.org/3/movie/';
		let API_URL = BASE_URL + 'top_rated?api_key=' + API_KEY;
		Movie.truncate();

		const movies: string[] = [];
		let i = 1;
		let movieIds: number[] = [];
		try {
			do {
				// if (i < 11) {
				if (i < 2) {
					const record = await axios.get(API_URL + `&page=${i}`);
					let data = record.data.results;
					data.forEach((element: any) => {
						movieIds.push(element.id);
					});
					movies.push(data);
					i++;
					// 	} else if (i == 11) {
				} else if (i == 2) {
					const record = await axios.get(API_URL + `&page=${i}`);
					let data = record.data.results;

					const newData = data.filter((element: any, index: any) => {
						return index < 10;
					});

					newData.forEach((element: any) => {
						movieIds.push(element.id);
					});
					movies.push(newData);
					i++;
				}
				// } while (i < 12);
			} while (i < 3);
			movieIds.forEach(id => {
				axios
					.get(`https://api.themoviedb.org/3/movie/${id}?api_key=bd9eb422bf109b790455909c7396f70d`)
					.then(response => {
						const titleName = response.data.title.replace(/\s/g, '-').toLowerCase();

						Movie.create({
							tmdbId: response.data.id,
							title: response.data.title,
							length: response.data.runtime,
							genres: response.data.genres,
							releaseDate: response.data.release_date,
							overview: response.data.overview,
							tmdbVoteAvarage: response.data.vote_average,
							tmdbVoteCount: response.data.vote_count,
							posterUrl: `https://image.tmdb.org/t/p/w500${response.data.poster_path}`,
							tmdbUrl: `https://www.themoviedb.org/movie/${response.data.id}-${titleName}`,
						});
					});

				axios
					.get(`https://api.themoviedb.org/3/movie/${id}/credits?api_key=bd9eb422bf109b790455909c7396f70d`)
					.then(response => {
						const crew = response.data.crew;

						crew.forEach((e: any) => {
							if (e.job === 'Director') {
								axios
									.get(`https://api.themoviedb.org/3/person/${e.id}?api_key=bd9eb422bf109b790455909c7396f70d`)
									.then(director => {
										Movie.update(
											{
												directorId: director.data.id,
												directorName: director.data.name,
												directorBio: director.data.biography,
												directorBirthday: director.data.birthday,
											},
											{ where: { tmdbId: id } }
										);
									});
							}
						});
					});
			});

			res.json(movies);
		} catch (error) {
			if (error instanceof Error) {
				return next(new CustomError(500, 'Internal server error'));
			}
			return null;
		}
	};

	getAllMovies = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const record = await Movie.findAll({});

			return res.json(record);
		} catch (error) {
			if (error instanceof Error) {
				return next(new CustomError(500, 'Internal server error'));
			}
			return null;
		}
	};
}

export default new MoviesController();
