"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Director = void 0;
const sequelize_1 = require("sequelize");
const dbconfig_1 = __importDefault(require("../config/dbconfig"));
class Director extends sequelize_1.Model {
}
exports.Director = Director;
Director.init({
    id: {
        type: sequelize_1.DataTypes.UUID,
        defaultValue: sequelize_1.DataTypes.UUIDV4,
        primaryKey: true,
    },
    directorId: {
        type: sequelize_1.DataTypes.INTEGER,
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
        defaultValue: null,
    },
    bio: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
        defaultValue: null,
    },
    birthday: {
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.DataTypes.NOW,
        validate: {
            isDate: {
                args: true,
                msg: 'Please enter correct date format',
            },
        },
    },
}, {
    tableName: 'director',
    underscored: true,
    timestamps: true,
    sequelize: dbconfig_1.default,
});
exports.default = Director;
