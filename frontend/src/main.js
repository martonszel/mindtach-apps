import 'vuetify/styles' // Global CSS has to be imported
import { createApp } from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import { loadFonts } from './plugins/webfontloader'
import router from "./router/index"
import { store } from './store'
import 'nprogress/nprogress.css'

loadFonts()

createApp(App)
    .use(store)
    .use(router)
    .use(vuetify)
    .mount('#app')
