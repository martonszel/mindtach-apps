"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const MoviesController_1 = __importDefault(require("../controllers/MoviesController"));
const router = (0, express_1.Router)();
router.get('/api/get-movies', MoviesController_1.default.findTopRatedMovies);
router.get('/api/movies', MoviesController_1.default.getAllMovies);
exports.default = router;
