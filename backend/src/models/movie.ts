import { DataTypes, Model } from 'sequelize';
import sequelizeConnection from '../config/dbconfig';

export class Movie extends Model {
	public tmdbId!: number;
	public title!: string;
	public length!: number;
	public genres!: string[];
	public releaseDate!: Date;
	public overview!: string;
	public tmdbVoteAvarage!: number;
	public tmdbVoteCount!: number;
	public posterUrl!: string;
	public tmdbUrl!: string;
	public directorId!: number;
	public directorName!: string;
	public directorBio!: string;
	public directorBirthday!: Date;

	// timestamps!
	public readonly createdAt!: Date;
	public readonly updatedAt!: Date;
	public readonly deletedAt!: Date;
}

Movie.init(
	{
		tmdbId: {
			type: DataTypes.INTEGER,
			primaryKey: true,
		},
		title: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: null,
		},

		length: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: null,
		},
		genres: {
			type: DataTypes.ARRAY(DataTypes.STRING),
			allowNull: true,
			defaultValue: null,
		},
		releaseDate: {
			type: DataTypes.DATE,
			allowNull: true,
			defaultValue: null,
			validate: {
				isDate: {
					args: true,
					msg: 'Please enter correct date format',
				},
			},
		},
		overview: {
			type: DataTypes.TEXT,
			allowNull: false,
			defaultValue: null,
		},
		tmdbVoteAvarage: {
			type: DataTypes.FLOAT,
			allowNull: false,
			defaultValue: null,
		},
		tmdbVoteCount: {
			type: DataTypes.INTEGER,
			allowNull: false,
			defaultValue: null,
		},
		posterUrl: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: null,
		},
		tmdbUrl: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: null,
		},

		directorId: {
			type: DataTypes.INTEGER,
		},
		directorName: {
			type: DataTypes.STRING,
			defaultValue: null,
		},

		directorBio: {
			type: DataTypes.TEXT,
			defaultValue: null,
		},

		directorBirthday: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			defaultValue: null,
			validate: {
				isDate: {
					args: true,
					msg: 'Please enter correct date format',
				},
			},
		},
	},

	{
		tableName: 'movie',
		underscored: true,
		timestamps: true,
		sequelize: sequelizeConnection,
		indexes: [
			{
				unique: true,
				fields: ['tmdb_id', 'director_id'],
			},
		],
	}
);

export default Movie;
