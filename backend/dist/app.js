"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const dotenv_1 = __importDefault(require("dotenv"));
const node_cron_1 = __importDefault(require("node-cron"));
const movies_1 = __importDefault(require("./src/routes/movies"));
const dbconfig_1 = __importDefault(require("./src/config/dbconfig"));
const Movie_1 = __importDefault(require("./src/models/Movie"));
const handleerror_1 = __importDefault(require("./src/middleware/handleerror"));
const path_1 = __importDefault(require("path"));
const axios_1 = __importDefault(require("axios"));
dotenv_1.default.config();
const app = (0, express_1.default)();
const port = process.env.PORT || 3200;
const corsOptions = {
    origin: 'http://localhost:8081',
};
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: true }));
app.use((0, cors_1.default)(corsOptions));
dbconfig_1.default
    .sync({ force: true })
    .then(() => {
    // { force: true }
    // { alter: true }
    Movie_1.default.sync();
    console.log('database synced');
})
    .catch(err => {
    console.log(`Error occurred: ${err}`);
});
app.use(movies_1.default);
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});
node_cron_1.default.schedule('* * * * *', () => {
    console.log('running a task every minute');
    const url = 'https://shrouded-journey-87943.herokuapp.com/api/get-movies';
    axios_1.default.get(url);
});
app.use(express_1.default.static(path_1.default.join(__dirname, '../../frontend/dist')));
app.get(/.*/, (req, res) => {
    res.sendFile(path_1.default.join(__dirname, '../../frontend/dist/index.html'));
});
app.use(handleerror_1.default);
app.get('*', (req, res) => {
    res.status(404).json({ msg: 'this route doesnt exist' });
});
