const add = (a: number, b: number) => {
	return a + b;
};

describe('this is a test', () => {
	it('should pass', () => {
		expect(add(1, 2)).toBe(3);
	});
});
