"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// eslint-disable-next-line
const errorHandler = (error, req, res, next) => {
    console.log(error);
    const status = error.status || 500;
    const message = error.message || 'Something went wrong';
    const route = req.path;
    const { stack } = error;
    const reqMethod = req.method;
    const reqIp = req.socket.remoteAddress;
    res.status(status).json({ status, message, route, stack });
};
exports.default = errorHandler;
