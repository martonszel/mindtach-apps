import { DataTypes, Model } from 'sequelize';
import sequelizeConnection from '../config/dbconfig';

export class Director extends Model {
	public id!: string;
	public directorId!: number;
	public name!: string;
	public bio!: string;
	public birthday!: Date;
	// timestamps!
	public readonly createdAt!: Date;
	public readonly updatedAt!: Date;
	public readonly deletedAt!: Date;
}

Director.init(
	{
		id: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
			primaryKey: true,
		},

		directorId: {
			type: DataTypes.INTEGER,
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: null,
		},
		bio: {
			type: DataTypes.TEXT,
			allowNull: false,
			defaultValue: null,
		},

		birthday: {
			type: DataTypes.DATE,
			defaultValue: DataTypes.NOW,
			validate: {
				isDate: {
					args: true,
					msg: 'Please enter correct date format',
				},
			},
		},
	},

	{
		tableName: 'director',
		underscored: true,
		timestamps: true,
		sequelize: sequelizeConnection,
	}
);

export default Director;
