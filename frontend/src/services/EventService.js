import axios from 'axios'
import NProgress from 'nprogress'

const apiClient = axios.create({
    baseURL: 'http://localhost:3200',
    //https://shrouded-journey-87943.herokuapp.com/
})


apiClient.interceptors.request.use(config => {
    NProgress.start()
    return config
})

apiClient.interceptors.response.use(response => {
    NProgress.done()
    return response
})

const getMovies = () => {
    return apiClient.get('api/movies')
};

export default apiClient

export {
    getMovies
}
