import { createStore } from "vuex";
import views from './modules/views';

export const store = createStore({
    state: {
    },
    mutations: {
    },
    actions: {
    },
    modules: {
        views
    }
})
