# mindtach-apps

Full stack app with a Nodej js backend and a Vue js frontend . Backend made with Typescript and Express . Frontend made with Vuex , Vite and Vuetify.
Testing framework is Jest.

This app creates a movie database in every 30 minutes from MovieDB Api and displays it in (https://shrouded-journey-87943.herokuapp.com/).
Becouse of the cronjob firstly you have to go to https://<span></span>shrouded-journey-87943.herokuapp.com/api/get-movies , it will get the movies.

## Getting started

- cd backend & npm i & npx nodemon
- cd frontend & npm i & npm run dev
- install pgAdmin

## Test and Deploy

- test : cd backend & npm run test
- build frontend : cd ftontend & npm run build & cd dist/assets & change https:localhost:3200 --> https://<span></span>shrouded-journey-87943.herokuapp.com/
- build backend : cd backend & npm run build & cd dist/src/config & change database credentials & commit & git push heroku main
- open https://shrouded-journey-87943.herokuapp.com/

---
