"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const Movie_1 = __importDefault(require("../models/Movie"));
const CustomError_1 = __importDefault(require("../utilities/CustomError"));
class MoviesController {
    constructor() {
        this.findTopRatedMovies = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let API_KEY = 'bd9eb422bf109b790455909c7396f70d';
            let BASE_URL = 'https://api.themoviedb.org/3/movie/';
            let API_URL = BASE_URL + 'top_rated?api_key=' + API_KEY;
            Movie_1.default.truncate();
            const movies = [];
            let i = 1;
            let movieIds = [];
            try {
                do {
                    if (i < 2) {
                        const record = yield axios_1.default.get(API_URL + `&page=${i}`);
                        let data = record.data.results;
                        data.forEach((element) => {
                            movieIds.push(element.id);
                        });
                        movies.push(data);
                        i++;
                    }
                    else if (i == 2) {
                        const record = yield axios_1.default.get(API_URL + `&page=${i}`);
                        let data = record.data.results;
                        const newData = data.filter((element, index) => {
                            return index < 10;
                        });
                        newData.forEach((element) => {
                            movieIds.push(element.id);
                        });
                        movies.push(newData);
                        i++;
                    }
                } while (i < 3);
                movieIds.forEach(id => {
                    axios_1.default
                        .get(`https://api.themoviedb.org/3/movie/${id}?api_key=bd9eb422bf109b790455909c7396f70d`)
                        .then(response => {
                        const titleName = response.data.title.replace(/\s/g, '-').toLowerCase();
                        Movie_1.default.create({
                            tmdbId: response.data.id,
                            title: response.data.title,
                            length: response.data.runtime,
                            genres: response.data.genres,
                            releaseDate: response.data.release_date,
                            overview: response.data.overview,
                            tmdbVoteAvarage: response.data.vote_average,
                            tmdbVoteCount: response.data.vote_count,
                            posterUrl: `https://image.tmdb.org/t/p/w500${response.data.poster_path}`,
                            tmdbUrl: `https://www.themoviedb.org/movie/${response.data.id}-${titleName}`,
                        });
                    });
                    axios_1.default
                        .get(`https://api.themoviedb.org/3/movie/${id}/credits?api_key=bd9eb422bf109b790455909c7396f70d`)
                        .then(response => {
                        const crew = response.data.crew;
                        crew.forEach((e) => {
                            if (e.job === 'Director') {
                                axios_1.default
                                    .get(`https://api.themoviedb.org/3/person/${e.id}?api_key=bd9eb422bf109b790455909c7396f70d`)
                                    .then(director => {
                                    Movie_1.default.update({
                                        directorId: director.data.id,
                                        directorName: director.data.name,
                                        directorBio: director.data.biography,
                                        directorBirthday: director.data.birthday,
                                    }, { where: { tmdbId: id } });
                                });
                            }
                        });
                    });
                });
                res.json(movies);
            }
            catch (error) {
                if (error instanceof Error) {
                    return next(new CustomError_1.default(500, 'Internal server error'));
                }
                return null;
            }
        });
        this.getAllMovies = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
            try {
                const record = yield Movie_1.default.findAll({});
                return res.json(record);
            }
            catch (error) {
                if (error instanceof Error) {
                    return next(new CustomError_1.default(500, 'Internal server error'));
                }
                return null;
            }
        });
    }
}
exports.default = new MoviesController();
