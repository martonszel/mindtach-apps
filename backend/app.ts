import express, { Request, Response } from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import cron from 'node-cron';
import moviesRoutes from './src/routes/movies';
import sequelizeConnection from './src/config/dbconfig';
import Movie from './src/models/Movie';
import errorHandler from './src/middleware/handleerror';
import path from 'path';
import axios from 'axios';

dotenv.config();

const app = express();
const port = process.env.PORT || 3200;

const corsOptions = {
	origin: 'http://localhost:8081',
};

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors(corsOptions));

sequelizeConnection
	.sync({ force: true })
	.then(() => {
		// { force: true }
		// { alter: true }
		Movie.sync();
		console.log('database synced');
	})
	.catch(err => {
		console.log(`Error occurred: ${err}`);
	});

app.use(moviesRoutes);

app.listen(port, () => {
	console.log(`server started at http://localhost:${port}`);
});

cron.schedule('* * * * *', () => {
	console.log('running a task every minute');
	const url = 'https://shrouded-journey-87943.herokuapp.com/api/get-movies';
	axios.get(url);
});

app.use(express.static(path.join(__dirname, '../../frontend/dist')));
app.get(/.*/, (req: Request, res: Response) => {
	res.sendFile(path.join(__dirname, '../../frontend/dist/index.html'));
});

app.use(errorHandler);

app.get('*', (req: Request, res: Response) => {
	res.status(404).json({ msg: 'this route doesnt exist' });
});
