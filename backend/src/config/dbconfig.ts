import { Dialect, Sequelize } from 'sequelize';
import * as dotenv from 'dotenv';

dotenv.config();

const dbName = process.env.DB_NAME as string;
const dbUser = process.env.DB_USER as string;
const dbHost = process.env.DB_HOST;
const dbPassword = process.env.DB_PASSWORD;
const dbDriver = process.env.DB_DRIVER as Dialect;

const sequelizeConnection = new Sequelize(dbName, dbUser, dbPassword, {
	host: dbHost,
	dialect: dbDriver,
	// logging: false,
});

export default sequelizeConnection;

// const dbName = 'ddddpm6fk8kour';
// const dbUser = 'avfxefvppwsftr';
// const dbHost = 'ec2-18-215-96-22.compute-1.amazonaws.com';
// const dbPassword = 'e09729f19a3d3386375ffe77633f6ebf226e746cae7ba47cb815354250edc4b7';
// const dbDriver = 'postgres';
// const dbport = '5432'

// const sequelizeConnection = new sequelize_1.Sequelize(dbName, dbUser, dbPassword, {
//     host: dbHost,
//     dialect: dbDriver,
//     port: dbport,
//     dialectOptions: {
//         ssl: {
//             rejectUnauthorized: false
//         }
//     }
//     // logging: false,
// });
